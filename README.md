# uQuery

A simple way to create simple yet easy to use command line experiences.
uQuery allows you both to query the user for data, and display user errors and warnings

## Example

```rust
use std::io;

fn main() -> io::Result<()> {
    let name = uquery::string("What is your name?")?;
    println!("Hello {}.", name);

    let day = uquery::string_with_default(
        "What is your favourite day?",
        Some("Monday"),
    )?;
    println!(r#"I see your favourite day is "{}"."#, day);

    let number: usize = uquery::parsable_with_default("Whats your favourite number?", 7)?;
    println!("Hello {} lover", number);

    let age: u8 = uquery::parsable("How old are you?")?;
    println!("Hello {} year old.", age);

    let happy = uquery::boolean("Are you happy?", None)?;
    println!("It is {} that you are happy.", happy);

    Ok(())
}
```
